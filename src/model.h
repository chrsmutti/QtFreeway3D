#ifndef MODEL_H
#define MODEL_H

#include <QOpenGLExtraFunctions>
#include <QVector2D>
#include <QVector4D>
#include <QMatrix4x4>
#include <QRect>

#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>

#include "shadermanager.h"
#include "material.h"

class Model : protected QOpenGLExtraFunctions {
public:
    QRectF boundingRect;
    QVector3D position;
    GLuint program;
    Material material;

protected:
    QVector4D color;
    QVector3D midPoint;
    QMatrix4x4 modelMatrix;
    float invDiag;
    float xScale = 1, yScale = 1;
    float angleX = 0, angleY = 0, angleZ = 0;
    float width, height;
    float z = 0;

    GLuint vao, vboIndices, vboPos, vboColor, vboNormals, vboTexture;

    QString texturePath;
    unsigned int nVert, nFaces;
    GLuint textureId;
    std::unique_ptr<QVector4D[]> vertices;
    std::unique_ptr<QVector3D[]> normals;
    std::unique_ptr<QVector2D[]> texture;
    std::unique_ptr<unsigned int[]> indices;

public:
    Model(const char *vertexPath, const char *fragmentPath, const char *offPath, QVector4D color, float xScale,
          float yScale, const char *texturePath);

    ~Model();

    void draw();

    void setPosition(float x, float y);

    virtual void translate(float x, float y);

private:
    void readOFF(QString const &path);

protected:
    void updateBoundingRect();

    void loadTexture();
};

#endif // MODEL_H
