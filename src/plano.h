#ifndef PLANO_H
#define PLANO_H

#include "model.h"
#include <QString>

class Plano : public Model
{
public:
    Plano(float x, float y, float z, float xScale, float yScale);
};

#endif // PLANO_H
