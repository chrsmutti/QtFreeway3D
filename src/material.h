//
// Created by cmutti on 11/25/17.
//

#ifndef QTFREEWAY3D_MATERIAL_H
#define QTFREEWAY3D_MATERIAL_H


#include <QtGui/QVector4D>

class Material {

public:
    Material();

    QVector4D ambient = QVector4D(.85, .85, .85, 1);
    QVector4D diffuse = QVector4D(0.7, 0.7, 0.8, 1);
    QVector4D specular = QVector4D(1, 1, 1, 1);

    float shininess = 20;

};


#endif //QTFREEWAY3D_MATERIAL_H
