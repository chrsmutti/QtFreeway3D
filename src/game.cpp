#include "game.h"


Game::Game(QOpenGLWidget *context) : context(context) {
    initializeOpenGLFunctions();
    running = true;
    dimensions = QVector2D(context->width(), context->height());
    backgroundColor = QVector3D(141, 141, 141);

    player = new Player(this);
    player->startPosition();
    deaths = score = 0;

    stage = 0;
    stagePath = QString(":/stages/stage%1").arg(stage);

    cars = new Car *[LANES];
    for (unsigned int i = 0; i < LANES; ++i)
        cars[i] = nullptr;

    cars = StageLoader::getInstance()->load(cars, stagePath, LANES, -4, 4);

    plano = new Plano(0, 0, 1, 20, 7);

    projectionMatrix.setToIdentity();
    viewMatrix.setToIdentity();
    viewMatrix.lookAt(eye, center, up);
    auto aspectRatio = static_cast<float>(this->dimensions.x() / this->dimensions.y());
    projectionMatrix.perspective(60, aspectRatio, 0.1, 20);

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
    timer.start(1000 / 60);
}

void Game::loop() {
    if (!running)
        return;

    context->makeCurrent();
    update();
    draw();
    context->update();
}

void Game::handleKey(int key) {
    if (player) {
        if (key == Qt::Key_Up)
            player->translate(0, 1);
        else if (key == Qt::Key_Down)
            player->translate(0, -1);
        else if (key == Qt::Key_Left)
            player->translate(1, 0);
        else if (key == Qt::Key_Right)
            player->translate(-1, 0);
    }
}

void Game::roundStart() {
    player->startPosition();

    QTimer timer;
    timer.singleShot(1000, this, SLOT(playerCanMove()));
}

void Game::playerCanMove() {
    player->canMove = true;
}

void Game::chickenDied() {
    static QTimer timer;
    timer.setSingleShot(true);

    if (timer.remainingTime() <= 0) {
        roundStart();
        deaths++;
        emit died(deaths);
        timer.start(200);
    }
}

void Game::chickenScored() {
    roundStart();
    score++;
    stage = (stage + 1) % STAGES;
    stagePath = QString(":/stages/stage%1").arg(stage);
    cars = StageLoader::getInstance()->load(cars, stagePath, LANES, -4, 4);
    emit scored(score);
}

void Game::draw() {
    glClearColor(backgroundColor.x() / 255.0, backgroundColor.y() / 255.0, backgroundColor.z() / 255.0, 1);

    this->setLightInfo(plano->program, plano);
    plano->draw();

    this->setLightInfo(player->program, player);
    player->draw();

    for (unsigned int i = 0; i < LANES; ++i) {
        if (cars[i]) {
            this->setLightInfo(cars[i]->program, cars[i]);
            cars[i]->draw();
        }
    }
}

void Game::setLightInfo(GLuint programId, Model *model) {
    auto ambientProduct = light.ambient * model->material.ambient;
    auto diffuseProduct = light.diffuse * model->material.diffuse;
    auto specularProduct = light.specular * model->material.specular;

    auto locmProjection = glGetUniformLocation(programId, "mProjection");
    auto locmView = glGetUniformLocation(programId, "mView");
    auto locvLight = glGetUniformLocation(programId, "vLight");
    auto locvAmbient = glGetUniformLocation(programId, "vAmbient");
    auto locvDiffuse = glGetUniformLocation(programId, "vDiffuse");
    auto locvSpecular = glGetUniformLocation(programId, "vSpecular");

    glUniformMatrix4fv(locmProjection, 1, GL_FALSE, projectionMatrix.data());
    glUniformMatrix4fv(locmView, 1, GL_FALSE, viewMatrix.data());
    glUniform4fv(locvLight, 1, &(light.position[0]));
    glUniform4fv(locvAmbient, 1, &(ambientProduct[0]));
    glUniform4fv(locvDiffuse, 1, &(diffuseProduct[0]));
    glUniform4fv(locvSpecular, 1, &(specularProduct[0]));
}

void Game::update() {
    static float cameraZoomTimes = 0;
    if (cameraZoomTimes > -7) {
        eye.setZ(cameraZoomTimes);

        viewMatrix.setToIdentity();
        viewMatrix.lookAt(eye, center, up);
        cameraZoomTimes -= 0.15;
    }

    for (unsigned int i = 0; i < LANES; ++i) {
        if (cars[i])
            cars[i]->move();
    }

    if (player->intersecting(cars, LANES))
        chickenDied();

}

Game::~Game() {
    delete player;
    delete plano;
    delete[] cars;
}
