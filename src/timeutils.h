#ifndef TIMEUTILS_H
#define TIMEUTILS_H

#include <QDateTime>

class TimeUtils {
public:
    double dt;

private:
    static TimeUtils *instance;
    long last = QDateTime::currentMSecsSinceEpoch();

public:
    static TimeUtils *getInstance();

    void updateDelta();

private:
    TimeUtils();
};

#endif // TIMEUTILS_H
