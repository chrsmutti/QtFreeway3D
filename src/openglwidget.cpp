#include "openglwidget.h"

OpenGLWidget::OpenGLWidget(QWidget *parent) : QOpenGLWidget(parent) {
}

void OpenGLWidget::initializeGL() {
    initializeOpenGLFunctions();

    qInfo("OpenGL Version: %s", glGetString(GL_VERSION));
    qInfo("GLSL Version: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

    game = new Game(this);

    connect(this, SIGNAL(keyEvent(int)), game, SLOT(handleKey(int)));
    connect(this, SIGNAL(drawCall()), game, SLOT(loop()));
    connect(game, SIGNAL(died(int)), this, SLOT(deathsChanged(int)));
    connect(game, SIGNAL(scored(int)), this, SLOT(scoreChanged(int)));
}

void OpenGLWidget::paintGL() {
    TimeUtils::getInstance()->updateDelta();
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(0, 0, 0, 1);

    emit drawCall();
}

void OpenGLWidget::resizeGL(int w, int h) {
    glViewport(0, 0, w, h);
}

void OpenGLWidget::keyPressEvent(QKeyEvent *event) {
    emit keyEvent(event->key());
}

void OpenGLWidget::scoreChanged(int score) {
    emit scored(score);
}

void OpenGLWidget::deathsChanged(int deaths) {
    emit died(deaths);
}

OpenGLWidget::~OpenGLWidget() = default;
