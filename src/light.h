//
// Created by cmutti on 11/25/17.
//

#ifndef QTFREEWAY3D_LIGHT_H
#define QTFREEWAY3D_LIGHT_H


#include <QtGui/QVector4D>

class Light {

public:
    Light();

    QVector4D position = QVector4D(15, 15, 15, 0);
    QVector4D ambient = QVector4D(1, 1, 1, 1);
    QVector4D diffuse = QVector4D(1, 1, 1, 1);
    QVector4D specular = QVector4D(1, 1, 1, 1);
};

#endif //QTFREEWAY3D_LIGHT_H
