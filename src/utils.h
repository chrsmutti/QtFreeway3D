//
// Created by cmutti on 11/25/17.
//

#ifndef QTFREEWAY3D_UTILS_H
#define QTFREEWAY3D_UTILS_H

#include <memory>
#include <QString>
#include <QFile>

class Utils {
public:
    static const QByteArray readFile(const char *path);

    static std::string readFileAsCStr(const char *path);

    static QImage readImage(const char* path);
};

#endif //QTFREEWAY3D_UTILS_H
