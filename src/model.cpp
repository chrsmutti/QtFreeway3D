#include <QtGui/QImage>
#include <cmath>
#include "model.h"
#include "utils.h"

Model::Model(const char *vertexPath, const char *fragmentPath, const char *offPath, QVector4D color, float xScale,
             float yScale, const char* texturePath) :
        color(color), xScale(xScale), yScale(yScale), texturePath(texturePath) {
    initializeOpenGLFunctions();
    readOFF(QString(offPath));

    program = ShaderManager::getInstance()->getProgram(vertexPath, fragmentPath);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    auto vert = vertices.get();
    auto norm = normals.get();
    auto indi = indices.get();
    auto text = texture.get();

    glGenBuffers(1, &vboPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glBufferData(GL_ARRAY_BUFFER, nVert * sizeof(QVector4D), vert, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &vboNormals);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
    glBufferData(GL_ARRAY_BUFFER, nVert * sizeof(QVector3D), norm, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &vboTexture);
    glBindBuffer(GL_ARRAY_BUFFER, vboTexture);
    glBufferData(GL_ARRAY_BUFFER, nVert * sizeof(QVector2D), text, GL_STATIC_DRAW);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &vboIndices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * nFaces * sizeof(unsigned int), indi, GL_STATIC_DRAW);
}

void Model::updateBoundingRect() {
    boundingRect = QRectF(
            QPointF((position.x() - (width / 2)), (position.y() - (width / 2))),
            QPointF((position.x() + (height / 2)), (position.y() + (height / 2))));
}

void Model::setPosition(float x, float y) {
    position = QVector2D(x, y);
}

void Model::translate(float x, float y) {
    if ((position.x() + x >= 9 && x > 0) || (position.x() + y <= -9 && x < 0))
        x = 0;
    if ((position.y() + y >= 9 && y > 0) || (position.y() + y <= -9 && y < 0))
        y = 0;

    position += QVector2D(x, y);
}

void Model::draw() {
    position.setZ(z);

    modelMatrix.setToIdentity();
    modelMatrix.scale(invDiag);
    modelMatrix.scale(xScale, yScale, 1);
    modelMatrix.translate(position);
    modelMatrix.rotate(angleX, 1, 0, 0);
    modelMatrix.rotate(angleY, 0, 1, 0);
    modelMatrix.rotate(angleZ, 0, 0, 1);
    modelMatrix.translate(-midPoint);

    glBindVertexArray(vao);
    glUseProgram(program);

    GLint locvColorIn = glGetUniformLocation(program, "vColorIn");
    glUniform4f(locvColorIn, color.x(), color.y(), color.z(), color.w());

    GLint locmModel = glGetUniformLocation(program, "mModel");
    glUniformMatrix4fv(locmModel, 1, GL_FALSE, modelMatrix.data());

    GLint locmNormal = glGetUniformLocation(program, "mNormal");
    glUniformMatrix4fv(locmNormal, 1, GL_FALSE, modelMatrix.normalMatrix().data());

    GLint locfShininess = glGetUniformLocation(program, "fShininess");
    glUniform1f(locfShininess, static_cast<GLfloat>(material.shininess));

    glDisable(GL_TEXTURE_2D);
    if (textureId) {
        GLint locColorTexture = 0;
        locColorTexture = glGetUniformLocation(program, "sTexture");
        glUniform1i(locColorTexture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    glDrawElements(GL_TRIANGLES, nFaces * 3, GL_UNSIGNED_INT, nullptr);
}

Model::~Model() {
    glDeleteBuffers(1, &vboIndices);
    glDeleteBuffers(1, &vboPos);
    glDeleteBuffers(1, &vboColor);
    glDeleteBuffers(1, &vboNormals);
    glDeleteBuffers(1, &vboTexture);
    glDeleteVertexArrays(1, &vao);

    vboIndices = 0;
    vboPos = 0;
    vboColor = 0;
    vboNormals = 0;
    vboTexture = 0;
    vao = 0;

    glDeleteProgram(program);
}

void Model::loadTexture() {
    if (texturePath.isEmpty())
        return;

    auto image = Utils::readImage(&texturePath.toStdString()[0]);

    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width(), image.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
}

void Model::readOFF(QString const &path) {
    auto byteArray = Utils::readFile(path.toStdString().c_str());

    if (byteArray.isNull()) {
        qWarning("Cannot open file");
        return;
    }

    std::istringstream stream;
    stream.str(byteArray.toStdString());
    std::string line;
    stream >> line;
    stream >> nVert >> nFaces >> line;

    vertices = std::make_unique<QVector4D[]>(nVert);
    indices = std::make_unique<unsigned int[]>(nFaces * 3);

    if (nVert > 0) {
        float minLim = std::numeric_limits<float>::lowest();
        float maxLim = std::numeric_limits<float>::max();

        QVector4D max(minLim, minLim, 1, 1), min(maxLim, maxLim, 1, 1);

        float x, y, z;
        for (unsigned int i = 0; i < nVert; i++) {
            stream >> x >> y >> z;

            max.setX(std::max(x, max.x()));
            max.setY(std::max(y, max.y()));
            max.setZ(std::max(z, max.z()));

            min.setX(std::min(x, min.x()));
            min.setY(std::min(y, min.y()));
            min.setZ(std::min(z, min.z()));

            vertices[i] = QVector4D(x, y, z, 1);
        }

        QVector4D nMin = max;
        QVector4D nMax = min;

        QVector4D normMin = QVector4D(abs(min.x()), abs(min.y()), abs(min.z()), 1);
        QVector4D normMax = max + normMin;

        for (unsigned int i = 0; i < nVert; i++) {
            vertices[i] += normMin;
            vertices[i] /= normMax;

            nMax.setX(std::max(vertices[i].x(), nMax.x()));
            nMax.setY(std::max(vertices[i].y(), nMax.y()));
            nMax.setZ(std::max(vertices[i].z(), nMax.z()));

            nMin.setX(std::min(vertices[i].x(), nMin.x()));
            nMin.setY(std::min(vertices[i].y(), nMin.y()));
            nMin.setZ(std::min(vertices[i].z(), nMin.z()));
        }

        midPoint = ((nMin + nMax) * 0.5).toVector3D();
        invDiag = ((xScale + yScale) / (xScale + yScale)) / (nMax - nMin).length();

        width = nMax.x() * xScale;
        height = nMax.y() * yScale;
    }

    unsigned int a, b, c;
    for (unsigned int i = 0; i < nFaces; i++) {
        stream >> line >> a >> b >> c;

        indices[i * 3 + 0] = a;
        indices[i * 3 + 1] = b;
        indices[i * 3 + 2] = c;
    }

    normals = std::make_unique<QVector3D[]>(nVert);

    for (unsigned int i = 0; i < nFaces; ++i) {
        auto d = QVector3D(vertices[indices[i * 3 + 0]]);
        auto e = QVector3D(vertices[indices[i * 3 + 1]]);
        auto f = QVector3D(vertices[indices[i * 3 + 2]]);
        auto faceNormal = QVector3D::crossProduct((e - d), (f - e));

        normals[indices[i * 3 + 0]] += faceNormal;
        normals[indices[i * 3 + 1]] += faceNormal;
        normals[indices[i * 3 + 2]] += faceNormal;
    }

    for (unsigned int i = 0; i < nVert; ++i) {
        normals[i].normalize();
    }

    texture = std::make_unique<QVector2D[]>(nVert);
    // Compute minimum and maximum values
    auto minz = std::numeric_limits<float>::max();
    auto maxz = std::numeric_limits<float>::lowest();

    for (unsigned int i = 0; i < nVert; ++i) {
        minz = std::min(vertices[i].z(), minz);
        maxz = std::max(vertices[i].z(), maxz);
    }

    if ((maxz - minz) > 0) {
        for (unsigned int i = 0; i < nVert; ++i) {
            auto s = (std::atan2(vertices[i].y(), vertices[i].x()) + M_PI) / (2 * M_PI);
            auto t = 1.0f - (vertices[i].z() - minz) / (maxz - minz);
            texture[i] = QVector2D(s, t);
        }
    } else {
        texture[3]= QVector2D(0 ,0);
        texture[2]= QVector2D(1 ,0);
        texture[1]= QVector2D(1 ,1);
        texture[0]= QVector2D(0 ,1);
    }

    loadTexture();
}
