#include "car.h"

Car::Car(QVector3D color, float y, float speed, int dir) :
        Model(":/shaders/vshader.glsl", ":/shaders/fshader.glsl", ":/offs/car3d.off", QVector4D(color / 255, 1), 1.5, 1,
              ":/textures/white.jpg"),
        speed(speed), dir(dir) {
    posMax = 15;
    setPosition(posMax * (-dir), y);

    angleZ = -90;
    angleY = -180;

    if (dir < 0) {
        angleZ -= 180;
    }
}

void Car::move() {
    translate(dir * speed * TimeUtils::getInstance()->dt, 0);
}

void Car::translate(float x, float y) {
    if ((position.x() + x >= posMax && x > 0) || (position.x() + x <= -posMax && x < 0))
        position.setX(posMax * (-dir));

    position += QVector2D(x, y);
    updateBoundingRect();
}
