#ifndef PLAYER_H
#define PLAYER_H

#include "model.h"
#include "timeutils.h"
#include "car.h"
#include "game.h"

class Game;

class Player : public Model {
public:
    Player(Game *game);

    void translate(float x, float y);

    bool intersecting(Car **cars, unsigned int size);

    void startPosition();

    bool canMove = true;

private:
    float velocity;
    float posMax;
    Game *game;

    void score();


};

#endif // PLAYER_H
