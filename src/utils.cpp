#include <memory>
#include <QtGui/QImage>
#include "utils.h"

const QByteArray Utils::readFile(const char *path) {
    QFile sf(path);
    sf.open(QFile::ReadOnly | QFile::Text);
    return sf.readAll();
}

std::string Utils::readFileAsCStr(const char *path) {
    auto byteArray = readFile(path);

    if (byteArray.isNull())
        return nullptr;

    return byteArray.toStdString();
}

QImage Utils::readImage(const char *path) {
    QImage image;
    image.load(path);
    return image.convertToFormat(QImage::Format_RGBA8888);
}
