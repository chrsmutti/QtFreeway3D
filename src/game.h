#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QOpenGLExtraFunctions>
#include <QOpenGLWidget>
#include <QDateTime>
#include <QVector2D>
#include <QVector3D>
#include <QTimer>

#include "player.h"
#include "timeutils.h"
#include "stageloader.h"
#include "light.h"
#include "plano.h"

#define LANES 7
#define STAGES 4

class Player;

class Game : public QObject, protected QOpenGLExtraFunctions {
Q_OBJECT
private:
    QOpenGLWidget *context;
    bool running = false;
    QVector2D dimensions;
    QVector3D backgroundColor;

    Player *player;
    Car **cars;
    Plano *plano;

    int deaths, score, stage;
    QString stagePath;

    Light light;
    QMatrix4x4 projectionMatrix;
    QMatrix4x4 viewMatrix;

    QVector3D eye = QVector3D(0, -2, 0);
    QVector3D center = QVector3D(0, 0, 0);
    QVector3D up = QVector3D(0, 1, 0);

public:
    Game(QOpenGLWidget *context);

    ~Game();

    void chickenDied();

    void chickenScored();

private:
    void draw();

    void roundStart();

    void setLightInfo(GLuint programId, Model *model);

public slots:

    void handleKey(int key);

    void loop();

    void update();

    void playerCanMove();

signals:

    void scored(int score);

    void died(int deaths);
};

#endif // GAME_H
