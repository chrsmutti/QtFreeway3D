#include "shadermanager.h"
#include "utils.h"

ShaderManager *ShaderManager::instance = nullptr;

ShaderManager::ShaderManager() {
    initializeOpenGLFunctions();
}

ShaderManager *ShaderManager::getInstance() {
    if (!instance)
        instance = new ShaderManager;

    return instance;
}

GLuint ShaderManager::createShader(const char *file, GLenum type) {
    auto content = Utils::readFileAsCStr(file);
    auto content_cstr = content.c_str();

    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &content_cstr, nullptr);
    glCompileShader(shader);

    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled == GL_FALSE) {
        GLchar message[1024];
        glGetShaderInfoLog(shader, 1024, nullptr, message);
        qInfo("Shader Compile Error: %s", message);

        return GL_FALSE;
    }

    return shader;
}

GLuint ShaderManager::createProgram(const GLuint vshader, const GLuint fshader) {
    GLuint program = glCreateProgram();

    glAttachShader(program, vshader);
    glAttachShader(program, fshader);
    glLinkProgram(program);

    GLint linked;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE) {
        GLchar message[1024];
        glGetProgramInfoLog(program, 1024, nullptr, message);
        qInfo("Program Linking Error: %s", message);

        return GL_FALSE;
    }

    glDeleteShader(vshader);
    glDeleteShader(fshader);
    return program;
}

GLuint ShaderManager::getProgram(const char *vshaderPath, const char *fshaderPath) {
    auto key = (QString(vshaderPath) + QString(fshaderPath)).toStdString();

    auto it = programs.find(key);
    if (it != programs.end())
        return it->second;

    auto vshader = createShader(vshaderPath, GL_VERTEX_SHADER);
    auto fshader = createShader(fshaderPath, GL_FRAGMENT_SHADER);

    auto program = createProgram(vshader, fshader);

    programs.emplace(std::make_pair(key, program));
    return program;
}
