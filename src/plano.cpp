#include "plano.h"

Plano::Plano(float x, float y, float z, float xScale, float yScale):
    Model(":/shaders/vshader.glsl", ":/shaders/fshader.glsl", ":/offs/plano.off", QVector4D(200, 200, 200, 1), xScale, yScale,
           ":/textures/road2.jpg")
{
    setPosition(x, y);
    this->z = z;
    this->material.ambient = QVector4D(0.02, 0.02, 0.02, 1);
}
