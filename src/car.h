#ifndef CAR_H
#define CAR_H

#include "model.h"
#include "timeutils.h"

class Car : public Model {
private:
    float speed;
    int dir;
    float posMax;

public:
    Car(QVector3D color, float y, float speed, int dir);

    void move();

    void translate(float x, float y);

};

#endif // CAR_H
