#include "player.h"

Player::Player(Game *game) : Model(":/shaders/vshader.glsl", ":/shaders/fshader.glsl", ":/offs/chicken3d.off",
                                   QVector4D(), 1, 1, ":/textures/chickenPlumage.jpg"), game(game) {
    posMax = 5;
    color = QVector3D(255, 252, 105) / 255;
    angleX = 180;
    angleY = 0;
    velocity = 1.0;
}

void Player::startPosition() {
    setPosition(0, -posMax);
}

void Player::translate(float x, float y) {
    if (!this->canMove)
        return;

    if ((position.x() + x >= posMax && x > 0) || (position.x() + y <= -posMax && x < 0))
        x = 0;
    if ((position.y() + y >= posMax + 2 && y > 0) || (position.y() + y <= -posMax && y < 0))
        y = 0;

    if (position.y() + y >= posMax + 1) {
        this->startPosition();
        this->score();
        return;
    }

    position += QVector2D(x * velocity * TimeUtils::getInstance()->dt, y * velocity * TimeUtils::getInstance()->dt);
    updateBoundingRect();
}

bool Player::intersecting(Car **cars, unsigned int size) {
    for (unsigned int i = 0; i < size; ++i) {
        if (cars[i] && cars[i]->boundingRect.intersects(this->boundingRect)) {
            return true;
        }
    }

    return false;
}

void Player::score() {
    game->chickenScored();
}
