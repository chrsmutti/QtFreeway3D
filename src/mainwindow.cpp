#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this);

    int id = QFontDatabase::addApplicationFont(QString(":/fonts/AtariFont.ttf"));
    QStringList list = QFontDatabase::applicationFontFamilies(id);

    ui->freewayLabel->setFont(QFont(list.first()));
    ui->deathsLabel->setFont(QFont(list.first()));
    ui->scoreLabel->setFont(QFont(list.first()));
    ui->timeLabel->setFont(QFont(list.first()));

    QPixmap image(QString(":/images/AtariLogo.png"));
    ui->imageLabel->setPixmap(image.scaled(80, 80));

    QTimer *timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(timeLapsed()));
    timer->start(1000);

    connect(ui->openGLWidget, SIGNAL(scored(int)), this, SLOT(scoreChanged(int)));
    connect(ui->openGLWidget, SIGNAL(died(int)), this, SLOT(deathsChanged(int)));
}

void MainWindow::scoreChanged(int score) {
    QString text("Score: %1");
    ui->scoreLabel->setText(text.arg(score));
}

void MainWindow::deathsChanged(int deaths) {
    QString text("Deaths: %1");
    ui->deathsLabel->setText(text.arg(deaths));
}

void MainWindow::timeLapsed() {
    static unsigned int time = 0;
    time++;
    QString text("Time: %1");
    ui->timeLabel->setText(text.arg(time));
}

MainWindow::~MainWindow() {
    delete ui;
}
