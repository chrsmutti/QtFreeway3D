#ifndef SHADERUTILS_H
#define SHADERUTILS_H

#include <QOpenGLExtraFunctions>
#include <QFile>

class ShaderManager : protected QOpenGLExtraFunctions {
private:
    static ShaderManager *instance;

    std::map<std::string, unsigned int> programs = std::map<std::string, unsigned int>();

    unsigned int createProgram(unsigned int vshader, unsigned int fshader);

    unsigned int createShader(const char *file, GLenum type);

public:
    static ShaderManager *getInstance();

    unsigned int getProgram(const char *vshaderPath, const char *fshaderPath);

private:
    ShaderManager();
};

#endif // SHADERUTILS_H
