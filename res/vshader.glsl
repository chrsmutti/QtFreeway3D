#version 400

layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoords;

uniform mat4 mModel;
uniform mat4 mView;
uniform mat4 mProjection;
uniform mat3 mNormal;
uniform vec4 vLight;
uniform vec4 vColorIn;

out vec4 vColor;
out vec3 fN;
out vec3 fE;
out vec3 fL;
out vec2 fTexCoords;

void main()
{
    vec4 VMvPosition = mView * mModel * vPosition;
    gl_Position = mProjection * VMvPosition;

    fN = mat3(mView) * mNormal * vNormal;
    fL = vLight.xyz - VMvPosition.xyz;
    fE = -VMvPosition.xyz;

    vColor = vColorIn * (vPosition.z + 0.2);
    fTexCoords = vTexCoords;
}
