#version 400

in vec4 vColor;
in vec2 fTexCoords;
in vec3 fN;
in vec3 fE;
in vec3 fL;

uniform vec4 vAmbient;
uniform vec4 vDiffuse;
uniform vec4 vSpecular;
uniform float fShininess;
uniform sampler2D sTexture;

out vec4 vFragColor;

vec4 Phong(vec3 n) {
    vec3 N = normalize(n);
    vec3 E = normalize(fE);
    vec3 L = normalize(fL);

    float NdotL = dot(N, L);

    vec3 R = normalize(2.0 * NdotL * N - L);

    float Kd = max(NdotL, 0.0);
    float Ks = (NdotL < 0.0) ? 0.0 : pow(max(dot(R, E), 0.0), fShininess);

    vec4 diffuse = Kd * vDiffuse;
    vec4 specular = Ks * vSpecular;
    vec4 ambient = vAmbient * vColor * texture2D(sTexture, fTexCoords);

    return ambient + diffuse + specular;
}

void main(void)
{
    vFragColor = Phong(fN);
    vFragColor.a = 1.0;
}
