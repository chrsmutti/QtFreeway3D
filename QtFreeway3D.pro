#-------------------------------------------------
#
# Project created by QtCreator 2017-11-18T09:38:39
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtFreeway3D
TEMPLATE = app

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    src/car.cpp \
    src/game.cpp \
    src/light.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/material.cpp \
    src/model.cpp \
    src/openglwidget.cpp \
    src/player.cpp \
    src/shadermanager.cpp \
    src/stageloader.cpp \
    src/timeutils.cpp \
    src/utils.cpp \
    src/plano.cpp

HEADERS += \
    src/car.h \
    src/game.h \
    src/light.h \
    src/mainwindow.h \
    src/material.h \
    src/model.h \
    src/openglwidget.h \
    src/player.h \
    src/shadermanager.h \
    src/stageloader.h \
    src/timeutils.h \
    src/utils.h \
    src/plano.h

FORMS += \
    src/mainwindow.ui

RESOURCES += \
    src/resources.qrc
