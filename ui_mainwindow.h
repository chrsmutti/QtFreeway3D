/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "src/openglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    OpenGLWidget *openGLWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *imageLabel;
    QSpacerItem *horizontalSpacer;
    QLabel *freewayLabel;
    QSpacerItem *horizontalSpacer_2;
    QLabel *scoreLabel;
    QSpacerItem *horizontalSpacer_4;
    QLabel *deathsLabel;
    QSpacerItem *horizontalSpacer_5;
    QLabel *timeLabel;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(812, 658);
        MainWindow->setStyleSheet(QLatin1String("QMainWindow {\n"
"	background-color: rgb(58, 55, 51);\n"
"}\n"
"\n"
"QLabel {\n"
"	color: rgb(224, 138, 98);\n"
"	font-size: 18px;\n"
"}\n"
""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        openGLWidget = new OpenGLWidget(centralWidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
        openGLWidget->setMinimumSize(QSize(800, 600));
        openGLWidget->setMaximumSize(QSize(800, 600));
        openGLWidget->setFocusPolicy(Qt::StrongFocus);
        openGLWidget->setStyleSheet(QLatin1String("QLabel {\n"
"	font-family: HammerFat, sans-serif;\n"
"}\n"
"\n"
"OpenGLWidget {\n"
"	border: 2px solid rgb(224, 138, 98); \n"
"}"));

        verticalLayout->addWidget(openGLWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        imageLabel = new QLabel(centralWidget);
        imageLabel->setObjectName(QStringLiteral("imageLabel"));
        imageLabel->setMaximumSize(QSize(80, 80));

        horizontalLayout->addWidget(imageLabel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        freewayLabel = new QLabel(centralWidget);
        freewayLabel->setObjectName(QStringLiteral("freewayLabel"));
        freewayLabel->setStyleSheet(QLatin1String("QLabel {\n"
"	font-size: 30px;\n"
"	color: rgb(247, 100, 90);\n"
"}"));

        horizontalLayout->addWidget(freewayLabel);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        scoreLabel = new QLabel(centralWidget);
        scoreLabel->setObjectName(QStringLiteral("scoreLabel"));

        horizontalLayout->addWidget(scoreLabel);

        horizontalSpacer_4 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        deathsLabel = new QLabel(centralWidget);
        deathsLabel->setObjectName(QStringLiteral("deathsLabel"));

        horizontalLayout->addWidget(deathsLabel);

        horizontalSpacer_5 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);

        timeLabel = new QLabel(centralWidget);
        timeLabel->setObjectName(QStringLiteral("timeLabel"));

        horizontalLayout->addWidget(timeLabel);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "FreeWay (Atari)", Q_NULLPTR));
        imageLabel->setText(QString());
        freewayLabel->setText(QApplication::translate("MainWindow", "Freeway", Q_NULLPTR));
        scoreLabel->setText(QApplication::translate("MainWindow", "Score: 0", Q_NULLPTR));
        deathsLabel->setText(QApplication::translate("MainWindow", "Deaths: 0", Q_NULLPTR));
        timeLabel->setText(QApplication::translate("MainWindow", "Time: 0", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
